﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RFIDUtilities.Models
{
    public class Reception
    {
        public string Code { get; set; }

        public bool IsConfirmed { get; set; }
        public DateTime? ReceivedOn { get; set; }

        public List<TagLine> Tags { get; set; }
        public List<TagLineRead> ReadTags { get; set; }

        public class TagLine
        {
            public string TagID { get; set; }
            public string Barcode { get; set; }
            public int StatusID { get; set; }
            public string Status { get; set; }
        }

        public class TagLineRead : TagLine
        {
            public DateTime ReceivedOn { get; set; }
            public bool ShouldSend { get; set; }
        }
    }

    public class ConfirmReception
    {
        [JsonProperty("sourceSystem", Order = 0)]
        public string SourceSystem { get; set; } = "RFID";

        [JsonProperty("receiptDate", Order = 1)]
        public DateTime ReceivedOn { get; set; }

        [JsonProperty("delivery", Order = 2)]
        public string Delivery { get; set; } = "delivery";

        [JsonProperty("codeID", Order = 3)]
        public string Code { get; set; }

        [JsonProperty("StoreID", Order = 4)]
        public string StoreID { get; set; }

        [JsonProperty("items", Order = 5)]
        public List<TagLine> Tags { get; set; }

        public class TagLine
        {
            [JsonProperty("EAN", Order = 0)]
            public string Barcode { get; set; }

            [JsonProperty("tagID", Order = 1)]
            public string TagID { get; set; }

            [JsonProperty("ReceivedOn", Order = 2)]
            public DateTime ReceivedOn { get; set; }

            [JsonProperty("UOM", Order = 3)]
            public string UOM { get; set; } = "1";
        }
    }

    public class PendingReceptions
    {
        public ObservableCollection<PendingReception> Rec { get; set; }

        public class PendingReception
        {
            public string Code { get; set; }
            public DateTime SentOn { get; set; }
            public string OriginID { get; set; }
            public string DestinationID { get; set; }

            public List<TagLine> RecTag { get; set; }
            public class TagLine
            {
                public int LineID { get; set; }
                public string TagID { get; set; }
                public string Barcode { get; set; }
                public int StatusID { get; set; }
            }
        }
    }
}