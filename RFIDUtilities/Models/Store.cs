﻿using System.Collections.ObjectModel;
using System.IO;

namespace RFIDUtilities.Models
{
    public class Store
    {
        public Store(string storeId, string storeName)
        {
            StoreId = storeId;
            StoreName = storeName;
        }

        public string StoreId { get; set; }
        public string StoreName { get; set; }
    }

    public class StoreList
    {
        public ObservableCollection<Store> Stores { get; set; }

        public StoreList()
        {
            Stores = new ObservableCollection<Store>();

            if (File.Exists("RFIDDES"))
                Stores.Add(new Store("0001", "Tienda Pruebas"));
            Stores.Add(new Store("0099", "L'Illa"));
            Stores.Add(new Store("0238", "Diagonal Mar"));
            Stores.Add(new Store("0267", "Maquinista"));
            Stores.Add(new Store("1819", "Glories"));
            Stores.Add(new Store("2471", "Canuda"));
        }
    }
}
