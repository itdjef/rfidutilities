﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RFIDUtilities.Models
{
    public class EndInventory
    {
        [JsonProperty("EndOn", Order = 0)]
        public DateTime EndOn { get; set; }
    }

    public class PendingInventories
    {
        public List<PendingInventory> Inventories { get; set; }

        public class PendingInventory
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public DateTime StartOn { get; set; }
            public string InSellZone { get; set; }
        }
    }
}