﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RFIDUtilities.Models
{
    public class SetDisplayQItems
    {
        [JsonProperty("qNew", Order = 0, NullValueHandling = NullValueHandling.Ignore)]
        public int QNew { get; set; }

        [JsonProperty("isExposed", Order = 1, NullValueHandling = NullValueHandling.Ignore)]
        public bool IsExposed { get; set; }

        [JsonProperty("modelColors", Order = 2, NullValueHandling = NullValueHandling.Ignore)]
        public List<string> ModelColors { get; set; }

        [JsonProperty("familyIDs", Order = 3, NullValueHandling = NullValueHandling.Ignore)]
        public List<string> FamilyIDs { get; set; }

    }
}