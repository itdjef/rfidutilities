﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using RFIDUtilities.Models;
using RFIDUtilities.Services.RFID;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using static RFIDUtilities.Models.PendingInventories;

namespace RFIDUtilities.ViewModels.Inventory
{
    public class RegularizeInventoryPageVM : BindableBase, INavigationAware
    {
        private ObservableCollection<Store> _stores = new ObservableCollection<Store>();
        public ObservableCollection<Store> Stores
        {
            get { return _stores; }
            set { SetProperty(ref _stores, value); }
        }

        private Store _selectedStore;
        public Store SelectedStore
        {
            get { return _selectedStore; }
            set { SetProperty(ref _selectedStore, value); }
        }

        private ObservableCollection<PendingInventory> _inventories = new ObservableCollection<PendingInventory>();
        public ObservableCollection<PendingInventory> Inventories
        {
            get { return _inventories; }
            set { SetProperty(ref _inventories, value); }
        }

        private PendingInventory _selectedInventory;
        public PendingInventory SelectedInventory
        {
            get { return _selectedInventory; }
            set { SetProperty(ref _selectedInventory, value); }
        }

        private string _btnRegularizeContent;
        public string BtnRegularizeContent
        {
            get { return _btnRegularizeContent; }
            set { SetProperty(ref _btnRegularizeContent, value); }
        }

        private string _lblWarningText;
        public string LblWarningText
        {
            get { return _lblWarningText; }
            set { SetProperty(ref _lblWarningText, value); }
        }

        private bool _cbInventorySelectorIsEnabled;
        public bool CbInventorySelectorIsEnabled
        {
            get { return _cbInventorySelectorIsEnabled; }
            set { SetProperty(ref _cbInventorySelectorIsEnabled, value); }
        }

        private bool _cbInventorySelectorIsDropDownOpen;
        public bool CbInventorySelectorIsDropDownOpen
        {
            get { return _cbInventorySelectorIsDropDownOpen; }
            set { SetProperty(ref _cbInventorySelectorIsDropDownOpen, value); }
        }

        private bool _isCancel = false;  //False --> Regularize Inventory, True --> Cancel Inventory

        public ICommand RegularizeCommand { get; set; }
        public ICommand StoreSelectorCommand { get; set; }
        private IDialogService _dialogService { get; set; }

        public RegularizeInventoryPageVM(IDialogService dialogService)
        {
            _dialogService = dialogService;
            Stores = new StoreList().Stores;

            RegularizeCommand = new DelegateCommand(btnRegularize_Click, btnRegularize_CanExecute).ObservesProperty(() => SelectedStore).ObservesProperty(() => SelectedInventory);
            StoreSelectorCommand = new DelegateCommand(StoreSelector_SelectionChanged);
        }

        private async void StoreSelector_SelectionChanged()
        {
            InitializeUI();

            MangoBUSService bus = MangoBUSService.Instance;
            try
            {
                PendingInventories pendingInventories = await bus.Inventories.GetPendingInventories("X" + SelectedStore.StoreId);
                InitializeUI();

                if (pendingInventories != null)
                {
                    var time = new TimeSpan(24, 0, 0);
                    foreach (PendingInventories.PendingInventory inventory in pendingInventories.Inventories)
                    {
                        if (DateTime.Now.Subtract(inventory.StartOn) < time)
                            Inventories.Add(inventory);
                    }
                }
                if (Inventories.Count == 0)
                    LblWarningText = SelectedStore.StoreName + " no tiene ningún inventario pendiente hoy.";

                if (Inventories.Count > 0)
                    CbInventorySelectorIsEnabled = true;

                if (Inventories.Count > 1)
                    CbInventorySelectorIsDropDownOpen = true;
            }
            catch(Exception ex)
            {
                LblWarningText = "Error al intentar recuperar el inventario a regularizar";
            }

        }
        private bool btnRegularize_CanExecute()
        {
            return SelectedStore != null && SelectedInventory != null;
        }

        private async void btnRegularize_Click()
        {
            bool? option = false;

            LblWarningText = string.Empty;

            string message = "Vas a " + (_isCancel ? "CANCELAR " : "regularizar ") + "el inventario "
                                              + SelectedInventory.Code
                                              + " de "
                                              + SelectedStore.StoreName
                                              + ".\nOK?";

            DialogParameters dialogParameters = new DialogParameters();
            dialogParameters.Add("message", message);
            dialogParameters.Add("title", "Aviso");

            _dialogService.ShowDialog("DialogBox", dialogParameters, r => {
                option = r.Result == ButtonResult.Yes;
            });

            if (option == true)
            {
                MangoBUSService bus = new MangoBUSService();

                EndInventory infoInv = new EndInventory() { EndOn = DateTime.Now };
                string invCode = await bus.Inventories.EndInventory((_isCancel ? "C" : "X") + SelectedStore.StoreId, SelectedInventory.Code, infoInv);
                LblWarningText = "Inventario " + SelectedInventory.Code + (_isCancel ? " cancelado." : " regularizado.");
                CbInventorySelectorIsEnabled = false;
            }
            else
            {
                LblWarningText = "Operación cancelada";
            }
        }

        private void InitializeUI()
        {
            LblWarningText = string.Empty;
            Inventories?.Clear();
            CbInventorySelectorIsEnabled = false;
        }

        #region Parameters
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            if(navigationContext.Parameters.ContainsKey("P1"))
            {
                _isCancel = (bool)navigationContext.Parameters["P1"];
            }

            if (_isCancel)
            {
                BtnRegularizeContent = "Cancelar Inventario";
            }
            else
            {
                BtnRegularizeContent = "Regularizar Inventario";
            }
            InitializeUI();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            return;
        }

        #endregion
    }
}
