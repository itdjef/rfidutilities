﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using RFIDUtilities.Models;
using RFIDUtilities.Services.RFID;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace RFIDUtilities.ViewModels.Receptions
{
    public class ConfirmReceptionsPageVM : BindableBase
    {
        private ObservableCollection<Store> _stores = new ObservableCollection<Store>();
        public ObservableCollection<Store> Stores
        {
            get { return _stores; }
            set { SetProperty(ref _stores, value); }
        }

        private Store _selectedStore;
        public Store SelectedStore
        {
            get { return _selectedStore; }
            set { SetProperty(ref _selectedStore, value); }
        }

        private ObservableCollection<Reception> _receptions = new ObservableCollection<Reception>();
        public ObservableCollection<Reception> Receptions
        {
            get { return _receptions; }
            set { SetProperty(ref _receptions, value); }
        }

        private Reception _selectedRception;
        public Reception SelectedReception
        {
            get { return _selectedRception; }
            set { SetProperty(ref _selectedRception, value); }
        }

        private ObservableCollection<Reception.TagLine> _receptionTags = new ObservableCollection<Reception.TagLine>();
        public ObservableCollection<Reception.TagLine> ReceptionTags
        {
            get { return _receptionTags; }
            set { SetProperty(ref _receptionTags, value); }
        }

        private string _lblWarningText;
        public string LblWarningText
        {
            get { return _lblWarningText; }
            set { SetProperty(ref _lblWarningText, value); }
        }

        private bool _cbReceptionSelectorIsEnabled;
        public bool CbReceptionSelectorIsEnabled
        {
            get { return _cbReceptionSelectorIsEnabled; }
            set { SetProperty(ref _cbReceptionSelectorIsEnabled, value); }
        }

        private bool _cbReceptionSelectorIsDropDownOpen;
        public bool CbReceptionSelectorIsDropDownOpen
        {
            get { return _cbReceptionSelectorIsDropDownOpen; }
            set { SetProperty(ref _cbReceptionSelectorIsDropDownOpen, value); }
        }

        public ICommand StoreSelectorCommand { get; set; }
        public ICommand ReceptionSelectorCommand { get; set; }
        public ICommand ConfirmCommand { get; set; }
        private IDialogService _dialogService { get; set; }

        private RFIDReceptionService bus = new RFIDReceptionService();
        private MangoBUSService mangoSrv = MangoBUSService.Instance;

        public ConfirmReceptionsPageVM(IDialogService dialogService)
        {
            _dialogService = dialogService;
            Stores = new StoreList().Stores;

            ConfirmCommand = new DelegateCommand(btnConfirmReception_Click, btnConfirmReception_CanExecute).ObservesProperty(() => SelectedStore).ObservesProperty(() => SelectedReception);
            StoreSelectorCommand = new DelegateCommand(StoreSelector_SelectionChanged);
            ReceptionSelectorCommand = new DelegateCommand(ReceptionSelector_SelectionChanged);
        }

        private async void StoreSelector_SelectionChanged()
        {
            InitializeUI();

            Receptions?.Clear();

            try
            {
                Receptions = await bus.GetPendingReceptions(SelectedStore.StoreId);

                if (Receptions == null || Receptions.Count == 0)
                    LblWarningText = SelectedStore.StoreName + " no tiene cajas por confirmar.";

                if (Receptions.Count > 0)
                    CbReceptionSelectorIsEnabled = true;

                if (Receptions.Count > 1)
                    CbReceptionSelectorIsDropDownOpen = true;
            }
            catch
            {
                LblWarningText = "Error al intentar recuperar las confirmaciones pendientes";
            }
        }

        private void InitializeUI()
        {
            LblWarningText = string.Empty;
            CbReceptionSelectorIsEnabled = false;
        }

        private void ReceptionSelector_SelectionChanged()
        {
            if (Receptions.Count == 0)
            {
                ReceptionTags = new ObservableCollection<Reception.TagLine>();
                return;
            }

            ReceptionTags = new ObservableCollection<Reception.TagLine>(bus.Receptions.Where(rc => rc.Code == SelectedReception.Code).FirstOrDefault().Tags);

            //if (cbBoxSelector.SelectedIndex > -1)
            //    btnConfirmBox.IsEnabled = true;
            //else
            //    btnConfirmBox.IsEnabled = false;

        }

        private bool btnConfirmReception_CanExecute()
        {
            return SelectedStore != null && SelectedReception != null;
        }

        private async void btnConfirmReception_Click()
        {
            bool? option = false;

            LblWarningText = string.Empty;

            string message = "Vas a confirmar el bulto " 
                                              + SelectedReception.Code
                                              + ".\nOK?";

            DialogParameters dialogParameters = new DialogParameters();
            dialogParameters.Add("message", message);
            dialogParameters.Add("title", "Aviso");

            _dialogService.ShowDialog("DialogBox", dialogParameters, r =>
            {
                option = r.Result == ButtonResult.Yes;
            });

            if (option == true)
            {

                ConfirmReception cr = new ConfirmReception()
                {
                    Code = SelectedReception.Code,
                    ReceivedOn = SelectedReception.ReceivedOn.GetValueOrDefault(),
                    StoreID = SelectedStore.StoreId,

                    Tags = bus.Receptions.Where(rc => rc.Code == SelectedReception.Code).FirstOrDefault().Tags.Select(t => new ConfirmReception.TagLine()
                    {
                        TagID = t.TagID,
                        Barcode = t.Barcode,
                        ReceivedOn = DateTime.Now    //((Reception)cbBoxSelector.SelectedItem).ReceivedOn.GetValueOrDefault()
                    }).ToList()
                };


                try
                {
                    await mangoSrv.Receptions.ConfirmReception(cr);
                    InitializeUI();
                    LblWarningText = "Caja " + SelectedReception.Code + " confirmada correctamente";
                    Receptions?.Clear();

                }
                catch (Exception ex)
                {
                    LblWarningText = "Error al intentar confirmar el bulto";
                }
            }
            else
            {
                LblWarningText = "Operación cancelada";
            }
        }
    }
}
