﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Windows;
using System.Windows.Controls;

namespace RFIDUtilities.ViewModels.Popups
{
    public class PasswordPageVM : BindableBase, IDialogAware
    {
        private string _lblWarningContent;
        public string lblWarningContent
        {
            get { return _lblWarningContent; }
            set { SetProperty(ref _lblWarningContent, value); }
        }

        private string _iconSource;
        public string IconSource
        {
            get { return _iconSource; }
            set { SetProperty(ref _iconSource, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public event Action<IDialogResult> RequestClose;

        public virtual void RaiseRequestClose(IDialogResult dialogResult)
        {
            RequestClose?.Invoke(dialogResult);
        }

        public virtual bool CanCloseDialog()
        {
            return true;
        }

        public virtual void OnDialogClosed()
        {

        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        public DelegateCommand<object> BtnOKCommand { get; set; }
        public DelegateCommand BtnCancelCommand { get; set; }

        public PasswordPageVM()
        {
            Title = "Aviso";
            BtnOKCommand = new DelegateCommand<object>(CloseOk);
            BtnCancelCommand = new DelegateCommand(CloseKo);
        }

        private void CloseOk(object parameter)
        {
            PasswordBox passwordBox = parameter as PasswordBox;
            if (passwordBox.Password == "RegulInv")
                RaiseRequestClose(new DialogResult(ButtonResult.Yes));
            else
                lblWarningContent = "Password incorrecto";
        }

        private void CloseKo()
        {
            Application.Current.Shutdown();
        }

        public virtual void OnDialogOpened(IDialogParameters parameters)
        {
            Message = parameters.GetValue<string>("message");
            Title = parameters.GetValue<string>("title");
        }
    }

}

