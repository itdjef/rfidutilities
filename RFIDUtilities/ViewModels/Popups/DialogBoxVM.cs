﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;

namespace RFIDUtilities.ViewModels.Popups
{
    public class DialogBoxVM : BindableBase, IDialogAware
    {
        private string _iconSource;
        public string IconSource
        {
            get { return _iconSource; }
            set { SetProperty(ref _iconSource, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public event Action<IDialogResult> RequestClose;

        public virtual void RaiseRequestClose(IDialogResult dialogResult)
        {
            RequestClose?.Invoke(dialogResult);
        }

        public virtual bool CanCloseDialog()
        {
            return true;
        }

        public virtual void OnDialogClosed()
        {

        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        public DelegateCommand BtnOkCommand { get; set; }
        public DelegateCommand BtnKoCommand { get; set; }

        public DialogBoxVM()
        {
            Title = "Aviso";
            BtnOkCommand = new DelegateCommand(CloseOk);
            BtnKoCommand = new DelegateCommand(CloseKo);
        }

        private void CloseOk()
        {
            RaiseRequestClose(new DialogResult(ButtonResult.Yes));
        }

        private void CloseKo()
        {
            RaiseRequestClose(new DialogResult(ButtonResult.No));
        }

        public virtual void OnDialogOpened(IDialogParameters parameters)
        {
            Message = parameters.GetValue<string>("message");
            Title = parameters.GetValue<string>("title");
        }
    }

}

