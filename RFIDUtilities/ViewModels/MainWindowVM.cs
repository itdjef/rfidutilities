﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using RFIDUtilities.Views.Popups;
using System.Windows.Input;

namespace RFIDUtilities.ViewModels
{
    public class MainWindowVM : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private readonly IDialogService _dialogService;
        public DelegateCommand<string> NavigateCommand { get; set; }
        public ICommand OpenPasswordDialogCommand { get; set; }

        public MainWindowVM(IRegionManager regionManager, IDialogService dialogService)
        {
            _regionManager = regionManager;
            _dialogService = dialogService;

            NavigateCommand = new DelegateCommand<string>(Navigate);
            OpenPasswordDialogCommand = new DelegateCommand(OpenPasswordDialog);
        }

        private void Navigate(string uri)
        {
            NavigationParameters parameters = new NavigationParameters();

            string[] paramNavigation = uri.Split(',');

            if (paramNavigation.Length > 1)
                if (paramNavigation[1] == "true" || paramNavigation[1] == "false")
                    parameters.Add("P1", bool.Parse(paramNavigation[1]));

            _regionManager.RequestNavigate("ContentRegion", paramNavigation[0], parameters);
        }

        private void OpenPasswordDialog()
        {
            _dialogService.ShowDialog("PasswordPage", new DialogParameters(), r => { });
        }
    }
}
