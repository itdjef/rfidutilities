﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using RFIDUtilities.Models;
using RFIDUtilities.Services.RFID;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace RFIDUtilities.ViewModels.Display
{
    public class InitializeDisplayPageVM : BindableBase
    {
        private ObservableCollection<Store> _stores = new ObservableCollection<Store>();
        public ObservableCollection<Store> Stores
        {
            get { return _stores; }
            set { SetProperty(ref _stores, value); }
        }

        private Store _selectedStore;
        public Store SelectedStore
        {
            get { return _selectedStore; }
            set { SetProperty(ref _selectedStore, value); }
        }

        private string _lblWarningText;
        public string LblWarningText
        {
            get { return _lblWarningText; }
            set { SetProperty(ref _lblWarningText, value); }
        }

        public ICommand CloseEyesCommand { get; set; }
        public ICommand OpenEyesCommand { get; set; }
        private IDialogService _dialogService { get; set; }

        public InitializeDisplayPageVM(IDialogService dialogService)
        {
            _dialogService = dialogService;
            Stores = new StoreList().Stores;

            CloseEyesCommand = new DelegateCommand(btnCloseEyes_Click, btnChangeEyes_CanExecute).ObservesProperty(() => SelectedStore);
            OpenEyesCommand = new DelegateCommand(btnOpenEyes_Click, btnChangeEyes_CanExecute).ObservesProperty(() => SelectedStore);
        }

        private bool btnChangeEyes_CanExecute()
        {
            return SelectedStore != null;
        }

        private void btnCloseEyes_Click()
        {
            OpenCloseEyes(false);
        }

        private void btnOpenEyes_Click()
        {
            OpenCloseEyes(true);
        }

        private void OpenCloseEyes(bool openClose)
        {
            LblWarningText = string.Empty;

            bool? option = false;

            LblWarningText = string.Empty;

            string message = "Vas a " + (openClose ? "ABRIR " : "CERRAR ") + "los ojos de la tienda "
                                              + SelectedStore.StoreName
                                              + ".\nOK?";

            DialogParameters dialogParameters = new DialogParameters();
            dialogParameters.Add("message", message);
            dialogParameters.Add("title", "Aviso");

            _dialogService.ShowDialog("DialogBox", dialogParameters, r =>
            {
                option = r.Result == ButtonResult.Yes;
            });

            if (option == true)
            {
                MangoBUSService bus = new MangoBUSService();

                var invCode = bus.Display.SetDisplayQ((openClose ? "C" : "X") + SelectedStore.StoreId);
                LblWarningText = "Operación de " + (openClose ? "ABRIR " : "CERRAR ") + " ojos realizada correctamente.";
            }
            else
            {
                LblWarningText = "Operación cancelada";
            }
        }
    }
}
