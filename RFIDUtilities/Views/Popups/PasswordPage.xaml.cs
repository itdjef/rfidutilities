﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RFIDUtilities.Views.Popups
{
    /// <summary>
    /// Lógica de interacción para PasswordPage.xaml
    /// </summary>
    public partial class PasswordPage : UserControl
    {
        public PasswordPage()
        {
            InitializeComponent();

            if (File.Exists("RFIDDES") || File.Exists("RFIDPRE"))
                pwBox.Background = new SolidColorBrush(Colors.BurlyWood);

        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
