﻿using System.Windows.Controls;

namespace RFIDUtilities.Views.Display
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class InitializeDisplayPage : UserControl
    {
        public InitializeDisplayPage()
        {
            InitializeComponent();
        }
    }
}
