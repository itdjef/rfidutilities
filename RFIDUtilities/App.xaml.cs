﻿using Prism.Ioc;
using Prism.Mvvm;
using Prism.Unity;
using RFIDUtilities.Views;
using RFIDUtilities.Views.Display;
using RFIDUtilities.Views.Inventory;
using RFIDUtilities.Views.Popups;
using RFIDUtilities.Views.Receptions;
using System;
using System.Reflection;
using System.Windows;

namespace RFIDUtilities
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App: PrismApplication
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<RegularizeInventoryPage>();
            containerRegistry.RegisterForNavigation<InitializeDisplayPage>();
            containerRegistry.RegisterForNavigation<ConfirmReceptionsPage>();

            containerRegistry.RegisterDialog<DialogBox>();
            containerRegistry.RegisterDialog<PasswordPage>();
        }

        //Configure default name of viewmodels, "VM" instead of "ViewModel"
        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();

            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
                var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
                var viewModelName = $"{viewName}VM, {viewAssemblyName}";
                return Type.GetType(viewModelName);
            });
        }
    }
}
