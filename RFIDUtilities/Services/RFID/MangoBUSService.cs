﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
//using RFIDUtilities.Utils.WebApi;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RFIDUtilities.Models;
using Utils.WebApi;

namespace RFIDUtilities.Services.RFID
{
    public class MangoBUSService
    {
        private static MangoBUSService instance = null;
        public static MangoBUSService Instance { get => instance = instance ?? new MangoBUSService(); }

        //private bool disposed = false;

        private readonly WebApiClient client = null;

        public MangoReceptions Receptions { get; }
        public MangoInventory Inventories { get; }
        public MangoDisplay Display { get; }

        private enum Environments { DEV, PRE, PRO};
        Environments environment = Environments.PRO;


        static MangoBUSService()
        {
            ServicePointManager.ServerCertificateValidationCallback += (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true;
        }

        public MangoBUSService()
        {
            string env = "p";
            string url = "https://{0}osb.mango.com/osb/api/retail/operations/rfid/"; 
            
            if (File.Exists("RFIDDES"))
                environment = Environments.DEV;

            if (File.Exists("RFIDPRE"))
                environment = Environments.PRE;

            if (File.Exists("RFIDPRO"))
                environment = Environments.PRO;

            switch (environment)
            {
                case Environments.DEV:
                    env = "d";
                    break;

                case Environments.PRE:
                    env = "i";
                    break;

                case Environments.PRO:
                    env = "p";
                    break;

                default:
                    env = "i";
                    break;
            }

            client = new WebApiClient(string.Format(url, env));

            if (File.Exists("bus10010.p12"))
            {

                try
                {
                    client.ClientHandler.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate2("bus10010.p12"));
                }
                catch (Exception exc)
                {
                    string prueba = "";
                    prueba += ";";
                }
            }

            Receptions = new MangoReceptions(client, env);
            Inventories = new MangoInventory(client, env);
            Display = new MangoDisplay(client, env);
        }

        public class MangoReceptions
        {
            private readonly WebApiClient client = null;

            string env = "p";

            public MangoReceptions(WebApiClient client, string environment = "p")
            {
                env = environment;
                this.client = client;
            }

            public async Task<ObservableCollection<PendingReceptions.PendingReception>> GetPendingReceptions(string storeID, int id = 0)
            {
                var pars = new WebApiParams() { EndUrl = "v1" };
                pars.AddParamsAsRoute("store");
                pars.AddParamsAsRoute(storeID);
                pars.AddParamsAsRoute("pending-receipt");

                return (await client.GetAsync<PendingReceptions>(pars))?.Rec;
            }

            public Task ConfirmReception(ConfirmReception rec)
            {

                var pars = new WebApiParams() { EndUrl = "v1" };
                pars.AddParamsAsRoute("store");
                pars.AddParamsAsRoute(rec.StoreID);
                pars.AddParamsAsRoute("package");
                pars.AddParamsAsRoute(rec.Code);
                pars.AddParamsAsRoute("receipt");

                return client.PostAsync<ConfirmReception, string>(pars, rec);
            }
        }

        public class MangoInventory
        {
            private readonly WebApiClient client = null;
            string env = "p";

            public MangoInventory(WebApiClient client, string environment = "p")
            {
                env = environment;
                this.client = client;
            }

            public async Task<PendingInventories> GetPendingInventories(string storeID)
            {
                var pars = new WebApiParams() { EndUrl = "v1" };
                pars.AddParamsAsRoute("store");
                pars.AddParamsAsRoute(storeID);
                pars.AddParamsAsRoute("inventory");

                return await client.GetAsync<PendingInventories>(pars);
            }

            public async Task<string> EndInventory(string storeID, string code, EndInventory info)
            {
                var pars = new WebApiParams() { EndUrl = "v1" };
                pars.AddParamsAsRoute("store");
                pars.AddParamsAsRoute(storeID);
                pars.AddParamsAsRoute("inventory");
                pars.AddParamsAsRoute(code);
                pars.AddParamsAsRoute("closing");

                return await client.PostAsync<EndInventory, string>(pars, info);
            }
        }

        public class MangoDisplay
        {
            private readonly WebApiClient client = null;
            string env = "p";

            public MangoDisplay(WebApiClient client, string environment = "p")
            {
                env = environment;
                this.client = client;
            }

            public Task<string> SetDisplayQ(string storeID)
            {
                SetDisplayQItems di = new SetDisplayQItems();

                var pars = new WebApiParams() { EndUrl = "v1" };
                pars.AddParamsAsRoute("store");
                pars.AddParamsAsRoute(storeID);
                pars.AddParamsAsRoute("display");

                return client.PostAsync<SetDisplayQItems, string>(pars, di);
            }
        }
    }
}
