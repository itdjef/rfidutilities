﻿using RFIDUtilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RFIDUtilities.Utils;
using System.Collections.ObjectModel;

namespace RFIDUtilities.Services.RFID
{
    public class RFIDReceptionService
    {
        private MangoBUSService mangoSrv = MangoBUSService.Instance;

        public ObservableCollection<Reception> Receptions { get; private set; }

        public async Task<ObservableCollection<Reception>> GetPendingReceptions(string storeID)
        {
            Receptions = new ObservableCollection<Reception>();
            try
            {
                ObservableCollection<PendingReceptions.PendingReception> remoteRecs = await mangoSrv.Receptions.GetPendingReceptions(storeID, 1);

                remoteRecs = remoteRecs ?? new ObservableCollection<PendingReceptions.PendingReception>();

                foreach (var remoteRec in remoteRecs)
                {
                    Reception rec = new Reception()
                    {
                        Code = remoteRec.Code,
                        IsConfirmed = false,
                        Tags = remoteRec.RecTag?.Select(rt => new Reception.TagLine() { TagID = rt.TagID, Barcode = rt.Barcode, StatusID = rt.StatusID,
                                                        Status = (rt.StatusID == Constants.StatusInWarehouse ? "Etiquetado" : 
                                                       (rt.StatusID == Constants.StatusPending ? "Pendiente" : 
                                                       (rt.StatusID == Constants.StatusConfirmed ? "Confirmado" : "Desconocido"))) }).ToList() ?? new List<Reception.TagLine>(),
                        ReadTags = remoteRec.RecTag?.Where(rt => rt.StatusID != Constants.StatusPending && rt.StatusID != Constants.StatusInWarehouse)
                                                    .Select(rt => new Reception.TagLineRead() { TagID = rt.TagID, Barcode = rt.Barcode, ReceivedOn = DateTime.Now, ShouldSend = false })
                                                    .ToList() ?? new List<Reception.TagLineRead>()
                    };
                    Receptions.Add(rec);
                }

                if (Receptions.Count() > 0)
                    OrderReceptions();
            }
            catch (Exception ex) { throw ex; }

            return Receptions;
        }

        public void OrderReceptions()
        {
            Receptions = new ObservableCollection<Reception>(Receptions.OrderByDescending(r =>
            {
                if (r.ReadTags.Count == 0 || r.Tags.Count == 0)
                    return -1;
                else if (r.ReadTags.Count == r.Tags.Count)
                    return 0;
                else
                    return Math.Round(r.ReadTags.Count * 100d / r.Tags.Count, 2);
            }).ToList());
        }

    }
}