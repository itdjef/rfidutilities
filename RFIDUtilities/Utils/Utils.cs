﻿namespace RFIDUtilities.Utils
{
    public static class Constants
    {
        public const int StatusInWarehouse = -1;
        public const int StatusPending = 0;
        public const int StatusConfirmed = 1;
        public const int StatusDeleted = 2;
    }
}
